# Flickr Challenge

> By Romaine Hinds

> 9/8/2016

## Objective
Build an application that calls on the Flickr API and shows photos based on the device’s current
location:

## Requirements
1. Use the device’s current location to query Flickr’s Image API.
2. Allow the ability to click on an image to zoom into it.
3. Utilize a local data store in some way.
4. Allow refreshing of the photo list.
5. Include some kind of animation.

## Timeline
The project is designed to take somewhere between 5 and 10 hours of heads­down time, but
this could vary depending on your experience level. Please try your best to submit it before the
weekend is over.

## Submission
Once complete, please share the link to the source code (e.g., github or bitbucket repo).

## Additional Notes
You are free to use any libraries or frameworks necessary to meet the objective. This project will
be assessed on two things. Design + Code Quality. Keep in mind aspects of separation of
concerns, SOLID principles, etc. when creating modules and organizing your codebase.
And if you have any questions along the way, you are welcome to ask!