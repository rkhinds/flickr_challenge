//
//  RealmPhotoData.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/9/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import RealmSwift

class RealmPhotoData: Object {
    
    dynamic var id: String!
    dynamic var mediumLink: String!
    dynamic var largeLink: String!
    dynamic var data: NSData!
    dynamic var page: String!
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
