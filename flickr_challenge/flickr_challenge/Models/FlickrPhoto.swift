//
//  FlickrPhoto.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/8/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlickrPhoto: Mappable {
    
     var id: String!
     var owner: String!
     var secret: String!
     var server: String!
     var farm: NSNumber!
     var title: String!
     var isPublic: NSNumber!
     var isFriend: NSNumber!
     var isFamily: NSNumber!
    
    init?(_ map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        owner <- map["owner"]
        secret <- map["secret"]
        server <- map["server"]
        farm <- map["farm"]
        title <- map["title"]
        isPublic <- map["isPublic"]
        isFamily <- map["isFamily"]
        isFriend <- map["isFriend"]
    }

    func getStaticImageLink(size: FlickrAPI.FlickrPhotoSize? = nil) -> String {
        guard let size = size else {
            return FlickrAPI.getLinkToStaticPhoto("\(farm)", serverId: "\(server)", id: id, secret: secret, size: .Medium)
        }
        return FlickrAPI.getLinkToStaticPhoto("\(farm)", serverId: "\(server)", id: id, secret: secret, size: size)
        
    }
}
