//
//  FlickrPhotoSearchResponse.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/8/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlickrPhotoSearchResponse: Mappable {
    
    var page: NSNumber!
    var pages: NSNumber!
    var perPage: NSNumber!
    var total: String!
    var photo: [FlickrPhoto]!
    var stat: String!
    
    init() {}
    init?(_ map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        page <- map["page"]
        pages <- map["pages"]
        perPage <- map["perpage"]
        total <- map["total"]
        photo <- map["photo"]
        stat <- map["stat"]
    }
}
