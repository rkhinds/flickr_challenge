//
//  FlickrPhotoRequest.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/8/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import ObjectMapper

class FlickrPhotoRequest: FlickrRequest, Mappable {
    
    var photo_id: String!
    
    init(method: String, photoId: String) {
        super.init(method: method)
        self.photo_id = photoId
    }
    
    required init?(_ map: Map) {
        super.init(method: map.JSONDictionary["method"] as! String)
    }
    
    func mapping(map: Map) {
        api_key <- map["api_key"]
        method <- map["method"]
        format <- map["format"]
        photo_id <- map["photo_id"]
    }
}