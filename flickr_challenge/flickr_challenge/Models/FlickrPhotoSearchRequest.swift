//
//  FlickrPhotoSearchRequest.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/8/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import ObjectMapper

class FlickrPhotoSearchRequest: FlickrRequest, Mappable {
    
    var contentType: Int = 1
    var has_geo: Int = 1
    var geo_context: Int = 0
    var lat: Double!
    var lon: Double!
    var radius: Int! = 10
    var radius_units: String! = "mi"
    var per_page: Int! = 250
    var page: Int! = 1
    
    init(useCurrentLocation: Bool = false) {
        super.init(method: FlickrAPI.PhotoSearchMethod)
        if useCurrentLocation {
            self.lat = LocationManager.sharedManager.currentLocation.coordinate.latitude
            self.lon = LocationManager.sharedManager.currentLocation.coordinate.longitude
        }
        
    }
    
    init(lat: Double, long: Double, radius: Int? = 10) {
        super.init(method: FlickrAPI.PhotoSearchMethod)
        self.lat = lat
        self.lon = long
        self.radius = radius
    }
    
    required init?(_ map: Map) {
        super.init(method: FlickrAPI.PhotoSearchMethod)
    }
    
    func mapping(map: Map) {
        api_key <- map["api_key"]
        method <- map["method"]
        format <- map["format"]
        contentType <- map["content-type"]
        has_geo <- map["has_geo"]
        geo_context <- map["geo_context"]
        lat <- map["lat"]
        lon <- map["lon"]
        radius <- map["radius"]
        radius_units <- map["radius_units"]
        page <- map["page"]
        per_page <- map["per_page"]
    }
    
}