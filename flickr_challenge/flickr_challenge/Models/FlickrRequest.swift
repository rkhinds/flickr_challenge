//
//  FlickrRequest.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/8/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation

class FlickrRequest {
    
    var method: String! 
    
    var api_key: String! = {
        return FlickrAPI.API_KEY
    }()
    
    var format: String! = {
        return "json"
    }()
    
    init(method: String) {
        self.method = method
    }
    
}