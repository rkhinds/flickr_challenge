//
//  FlickrPhotoGeoLocationData.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/8/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlickrPhotoGeoLocationData: Mappable {
    
     var id: String!
     var location: Location!
    
    init() {}
    init?(_ map: Map) {
        
    }
    
//    override static func primaryKey() -> String? {
//        return "id"
//    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        location <- map["location"]
    }
    
}

struct Location: Mappable {
    
    var latitude: String!
    var longitude: String!
    var accuracy: String!
    var context: String!
    var neighbourhood: Place!
    var county: Place!
    var region: Place!
    var country: Place!
    var place_id: String!
    var woeid: String!
    
    init?(_ map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        accuracy <- map["accuracy"]
        context <- map["context"]
        neighbourhood <- map["neighbourhood"]
        county <- map["county"]
        region <- map["region"]
        country <- map["country"]
        place_id <- map["place_id"]
        woeid <- map["woeid"]
    }
    
    
    struct Place: Mappable {
        
        var _content: String!
        var place_id: String!
        var woeid: String!
        
        init?(_ map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            _content <- map["_content"]
            place_id <- map["place_id"]
            woeid <- map["woeid"]
        }
    }
}
