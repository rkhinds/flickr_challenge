//
//  FlickrAPIManager.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/8/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class FlickrAPIManager {
    
    static let sharedInstance = FlickrAPIManager()
    
    private init() {}
    
    enum FlickrMethod {
        case PhotoSearch, GeoLocation, PhotoInfo
    }
    
    func getPhotoSearchResult(request: FlickrPhotoSearchRequest, success: ((FlickrPhotoSearchResponse?, NSError?) -> ())?, failure: (NSError? -> ())?) {
        let params = Mapper().toJSON(request)
        let url = FlickrAPI.BASE_URL
        Alamofire.request(.GET, url, parameters: params, encoding: .URL).responseJSON {
            response in
            let results = self.transformFlickrResponseData(response.data!, keyPath: "photos", type: FlickrPhotoSearchResponse())
            guard let result = results else {
                failure?(response.result.error)
                return
            }
            success?(result, nil)
        }
    }
    
    func getPhotoGeoLocationResult(request: FlickrPhotoRequest, success: ((FlickrPhotoGeoLocationData, NSError?) -> ())?, failure: (NSError? -> ())?) {
        let params = Mapper().toJSON(request)
        let url = FlickrAPI.BASE_URL
        Alamofire.request(.GET, url, parameters: params, encoding: .URL).responseJSON {
            response in
            let results = self.transformFlickrResponseData(response.data!, keyPath: "photo", type: FlickrPhotoGeoLocationData())
            guard let result = results else {
                failure?(response.result.error)
                return
            }
            success?(result, nil)
            
        }
        
        
    }
    
    func getPhotoInfoResult(request: FlickrPhotoRequest, success: ((FlickrPhotoInfo, NSError?) -> ())?, failure: (NSError? -> ())?) {
        let params = Mapper().toJSON(request)
        let url = FlickrAPI.BASE_URL
        Alamofire.request(.GET, url, parameters: params, encoding: .URL).responseJSON {
            response in
            let results = self.transformFlickrResponseData(response.data!, keyPath: "photo", type: FlickrPhotoInfo())
            guard let result = results else {
                failure?(response.result.error)
                return
            }
            success?(result, nil)
            
        }
        
    }
    
    func fetchFlickrPhoto(url: String, success: ((NSData?, NSError?) -> ())?, failure: (NSError? -> ())?) {
        Alamofire.request(.GET, url).responseJSON {
            response in
            NSLog("\(response.request)")
            NSLog("\(response.response)")
            guard let data = response.data else {
                failure?(response.result.error)
                return
            }
            success?(data, nil)
        }
    }
    
}

extension FlickrAPIManager {
 
    private func transformFlickrResponseData<T: Mappable>(data: NSData, keyPath: String, type: T) -> T? {
        let str = String(data: data, encoding: NSUTF8StringEncoding)
        let jsonString = self.flickrPhotoSearchSubString(str!)
        let json = self.convertStringToDictionary(jsonString)
        let results = Mapper<T>().map(json![keyPath])
        guard let res = results else {
            return nil
        }
        //print("Mapper Results:\n\(result)")
        return res
    }
    
    private func flickrPhotoSearchSubString(string: String) -> String {
        return string[string.startIndex.advancedBy(14)..<string.endIndex.advancedBy(-1)]
    }
    
    private func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
}