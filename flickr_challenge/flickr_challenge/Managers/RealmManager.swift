//
//  RealmManager.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/9/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    
    static let sharedManager = RealmManager()
    private init() {}
    
    func getRealm() -> Realm? {
        setDefaultRealm()
        var defRealm: Realm?
        do {
            defRealm = try Realm()
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
        return defRealm
    }
    
    func addPhotoToRealm(photo: FlickrPhoto, data: NSData, page: Int) {
        let realmObj = RealmPhotoData()
        realmObj.id = photo.id
        realmObj.mediumLink = photo.getStaticImageLink()
        realmObj.largeLink = photo.getStaticImageLink(.Large)
        realmObj.data = data
        realmObj.page = "\(page)"
        writePhotoToRealm(realmObj)
    }
    
    func getPhotosFromRealm(page: String) -> [RealmPhotoData]? {
        guard let realm = getRealm() else {
            return nil
        }
        var results: [RealmPhotoData]!
        autoreleasepool {
            let photos = realm.objects(RealmPhotoData.self).filter("page == %@", page)
            if photos.count > 0 {
                results = []
                for i in photos {
                    results.append(i)
                }
            }
        }
        return results
    }
    
    private func writePhotoToRealm(obj: RealmPhotoData) {
        guard let realm = getRealm() else {
            return
        }
        do {
            try realm.write {
                realm.add(obj, update: true)
            }
        }catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
    private func setDefaultRealm() {
        var config = Realm.Configuration()
        // Use the default directory, but replace the filename with the username
        config.fileURL = config.fileURL!.URLByDeletingLastPathComponent?
            .URLByAppendingPathComponent("default.realm")
        // Set this as the configuration used for the default Realm
        Realm.Configuration.defaultConfiguration = config
    }
}