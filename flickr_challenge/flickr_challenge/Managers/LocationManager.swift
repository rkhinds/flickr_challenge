//
//  LocationManager.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/9/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    
    static let sharedManager = LocationManager()
    
    private var locationManager:CLLocationManager!
    var currentLocation: CLLocation! {
        didSet {
            print("Current Location Updated!")
            NSNotificationCenter.defaultCenter().postNotificationName(kDidStartUpdatingLocationNotification, object: nil)
        }
    }
    
    var didStartUpdatingLocation: Bool!
    
    func startLocationManager() -> LocationManager? {
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        NSLog("Starting Location Manager Service.")
        
        if let _ = didStartUpdatingLocation {
            NSLog("Location Manager is already running.")
            return self
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            locationManager.requestAlwaysAuthorization()
        }else{
            didStartUpdatingLocation = false
            return nil
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }else{
            didStartUpdatingLocation = false
            return nil
        }
        
        didStartUpdatingLocation = true
        return self
    }
    
    func stopLocationManager() {
        if self.didStartUpdatingLocation! {
            guard let locationManager = self.locationManager else {
                NSLog("Location Manager has not been initialized nothing to stop.")
                return
            }
            NSLog("Stopping location Manager.")
            locationManager.stopUpdatingLocation()
            self.locationManager = nil
            self.didStartUpdatingLocation = false
        }
    }
    
    private override init() {
        super.init()
        NSLog("Shared Mobile Service Location Manager initialized.")
        
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedAlways || status == .AuthorizedWhenInUse {
            guard let locationManager = self.locationManager else {
                return
            }
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.first!
        self.didStartUpdatingLocation = true
        self.stopLocationManager()
    }
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        NSNotificationCenter.defaultCenter().postNotificationName(kDidFailToUpdateLocationNotification, object: error)
    }
    
}