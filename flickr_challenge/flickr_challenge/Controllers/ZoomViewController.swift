//
//  ZoomViewController.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/9/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

class ZoomViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    var imageView: UIImageView!
    var image: UIImage!
    var maskLayer: CAShapeLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupZoonView()
        // Do any additional setup after loading the view.
        maskLayer = CAShapeLayer()
        maskLayer.position = CGPoint(x: view.layer.bounds.size.width/2, y: view.layer.bounds.size.height/2)
        view.layer.mask = maskLayer
        navigationItem.title = "Zoom"
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        view.layer.mask = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupZoonView() {
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0
        scrollView.delegate = self
        scrollView.backgroundColor = UIColor.blackColor()
        scrollView.alwaysBounceVertical = false
        scrollView.alwaysBounceHorizontal = false
        scrollView.showsVerticalScrollIndicator = true
        scrollView.showsHorizontalScrollIndicator = true
        scrollView.flashScrollIndicators()
        self.view.addSubview(scrollView)
        imageView = UIImageView(frame: scrollView.frame)
        imageView.image = image
        scrollView.addSubview(imageView)
    }

}

extension ZoomViewController: UIScrollViewDelegate {
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}
