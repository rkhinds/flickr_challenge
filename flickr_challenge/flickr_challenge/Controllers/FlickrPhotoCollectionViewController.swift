//
//  FlickrPhotoCollectionViewController.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/9/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

private let reuseIdentifier = "photoCell"
private let headerReuseIdentifier = "headerView"
private let footerReuseIdentifier = "footerView"

class FlickrPhotoCollectionViewController: UICollectionViewController {

    private var flickrPhotoResponse: FlickrPhotoSearchResponse!
    private var flickrPhotos: [FlickrPhoto]!
    private var total: Int!
    private var updated: Bool = false
    private var currentPage: Int = 1
    
    private var queue: NSOperationQueue!
    private var refeash = UIRefreshControl()
    private var selectedImage: UIImage!
    
    let transition = RevealAnimator()
    var maskLayer: CAShapeLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        self.navigationController?.delegate = self

        // Do any additional setup after loading the view.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FlickrPhotoCollectionViewController.fetchPhotoOnLocationUpdate), name: kDidStartUpdatingLocationNotification, object: nil)
        
        navigationItem.title = "Flickr Photos"
        self.addPullToRefreash()
        maskLayer = CAShapeLayer()
        
        
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        view.layer.mask = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "zoomSegue" {
            let dest = segue.destinationViewController as! ZoomViewController
            dest.image = selectedImage
        }
    }
 

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if flickrPhotoResponse == nil {
            return 0
        }
        if let count = flickrPhotoResponse.perPage {
            return Int(count)
        }else{
            return 0
        }
    }

    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier, forIndexPath: indexPath) as! HeaderCollectionReusableView
            guard let flickrPhotoResponse = flickrPhotoResponse else {
                header.pageCountLabel.text = "Loading..."
                return header
            }
            header.pageCountLabel.text = "Page \(flickrPhotoResponse.page) of \(flickrPhotoResponse.pages)"
            return header
        case UICollectionElementKindSectionFooter:
            let footer = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionFooter, withReuseIdentifier: footerReuseIdentifier, forIndexPath: indexPath) as! FooterCollectionReusableView
            return footer
        default:
            return UICollectionReusableView()
        }
        
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! PhotoCollectionViewCell
        if indexPath.row >= flickrPhotos.count {
            let alert = UIAlertController(title: "Failed to load page \(self.currentPage)", message: nil, preferredStyle: .Alert)
            let dismiss = UIAlertAction(title: "dismiss", style: .Destructive, handler: {
                _ in
                alert.dismissViewControllerAnimated(true, completion: nil)
            })
            alert.addAction(dismiss)
            self.presentViewController(alert, animated: true, completion: nil)
            return cell
        }
        
        
        let photo = flickrPhotos[indexPath.row]
        fetchImageInBackground(photo.getStaticImageLink()) {
            data in
            let image = UIImage(data: data)
            cell.imageView.image = image
            self.updateRealm(self.currentPage, data: data, photo: photo)
        }
        return cell
        
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let photo = flickrPhotos[indexPath.row]
        fetchImageInBackground(photo.getStaticImageLink(.Large)) {
            data in
            self.selectedImage = UIImage(data: data)
            self.performSegueWithIdentifier("zoomSegue", sender: self)
        }
    }

    // MARK: UICollectionViewDelegate

    
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
 

    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
 

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}

extension FlickrPhotoCollectionViewController {
    
    func addPullToRefreash() {
        refeash = UIRefreshControl()
        refeash.addTarget(self, action: #selector(FlickrPhotoCollectionViewController.refresh), forControlEvents: .ValueChanged)
        self.collectionView!.addSubview(refeash)
    }
    
    func refresh() {
        LocationManager.sharedManager.startLocationManager()
        self.updated = false
        self.fetchPhotoOnLocationUpdate()
        refeash.endRefreshing()
        self.collectionView?.resetScrollPositionToTop()
    }
    
    func fetchPhotoOnLocationUpdate() {
        if updated {
            return
        }
        let request = FlickrPhotoSearchRequest(useCurrentLocation: true)
        request.page = currentPage
        FlickrAPIManager.sharedInstance.getPhotoSearchResult(request, success: {
            result, error in
            guard let res = result else {
                NSLog("FAILED to get Photo Search Results.")
                return
            }
            NSLog("Successfully got Photo Search Results.")
            self.flickrPhotoResponse = res
            self.flickrPhotos = res.photo
            self.total = Int(res.total)
            self.updated = true
            self.collectionView?.reloadData()
            }, failure: {
                error in
                NSLog("Failed to get photo search request! Error: \(error)")
                self.updated = false
        })

    }
    
    func fetchFlickrPhotos(shouldLoadMore: Bool = false, completion: (() -> ())? = nil) {
        navigationItem.title = "loading..."
        if !shouldLoadMore {
            return
        }
        
        let request = FlickrPhotoSearchRequest(useCurrentLocation: true)
        request.page = currentPage
        FlickrAPIManager.sharedInstance.getPhotoSearchResult(request, success: {
            result, error in
            guard let res = result else {
                NSLog("FAILED to get Photo Search Results.")
                return
            }
            NSLog("Successfully got Photo Search Results.")
            guard let photos = res.photo else {
                let alert = UIAlertController(title: "Failed to load page \(self.currentPage)", message: nil, preferredStyle: .Alert)
                let dismiss = UIAlertAction(title: "dismiss", style: .Destructive, handler: {
                    _ in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                })
                alert.addAction(dismiss)
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
            self.flickrPhotoResponse = res
            
            self.flickrPhotos = photos
            self.total = Int(res.total)
            self.currentPage += 1
            guard let completion = completion else {
                return
            }
            completion()
            }, failure: {
                error in
                NSLog("Failed to get photo search request! Error: \(error)")
                self.updated = false
        })
        
    }
    
    func updateRealm(page: Int, data: NSData, photo: FlickrPhoto) {
        let mgr = RealmManager.sharedManager
        mgr.addPhotoToRealm(photo, data: data, page: page)
    }
    
    func fetchImageInBackground(link: String, completion: (NSData) -> ()) {
        queue = NSOperationQueue()
        queue.addOperationWithBlock({
            FlickrAPIManager.sharedInstance.fetchFlickrPhoto(link, success: {
                data, error in
                guard let data = data else {
                    NSLog("Failed to download image with link: \(link)")
                    NSLog("Error: \(error)")
                    return
                }
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    completion(data)
                }
                }, failure: {
                    error in
                    NSLog("Failed to fetchImageInBackground. Error: \(error)")
            })
        })


    }
}

extension FlickrPhotoCollectionViewController {
    
    
    @IBAction func nextPageAction(sender: AnyObject) {
        fetchFlickrPhotos(true){
            self.collectionView?.reloadSections(NSIndexSet(index: 0))
            self.collectionView?.resetScrollPositionToTop()
            self.navigationItem.title = "Flickr Photos"
        }
    }
    
}

extension FlickrPhotoCollectionViewController: UINavigationControllerDelegate {
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.operation = operation
        return transition
    }
}
