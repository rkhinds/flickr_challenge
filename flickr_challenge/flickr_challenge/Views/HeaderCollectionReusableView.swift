//
//  HeaderCollectionReusableView.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/9/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var pageCountLabel: UILabel!
    
}
