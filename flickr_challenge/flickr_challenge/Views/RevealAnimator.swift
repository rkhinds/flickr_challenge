//
//  RevealAnimator.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/9/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit

class RevealAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let animationDuration = 0.60
    var operation: UINavigationControllerOperation = .Push
    weak var storedContext: UIViewControllerContextTransitioning?
    
    func transitionDuration(transitionContext:
        UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return animationDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        if operation == .Push {
            
            storedContext = transitionContext
            
            let fromVC = transitionContext.viewControllerForKey(
                UITransitionContextFromViewControllerKey) as!
            FlickrPhotoCollectionViewController
            let toVC = transitionContext.viewControllerForKey(
                UITransitionContextToViewControllerKey) as!
            ZoomViewController
            
            transitionContext.containerView()?.addSubview(toVC.view)
            
            let animation = CABasicAnimation(keyPath: "transform")
            animation.fromValue = NSValue(CATransform3D:
                CATransform3DIdentity)
            animation.toValue = NSValue(CATransform3D:
                CATransform3DConcat(
                    CATransform3DMakeTranslation(0.0, -10.0, 0.0),
                    CATransform3DMakeScale(150.0, 150.0, 1.0)
                )
            )
            
            animation.duration = animationDuration
            animation.delegate = self
            animation.fillMode = kCAFillModeForwards
            animation.removedOnCompletion = false
            animation.timingFunction = CAMediaTimingFunction(name:
                kCAMediaTimingFunctionEaseIn)
            
            toVC.maskLayer.addAnimation(animation, forKey: nil)
            fromVC.maskLayer.addAnimation(animation, forKey: nil)
        }else{
            
            storedContext = transitionContext
            
            let fromVC = transitionContext.viewControllerForKey(
                UITransitionContextFromViewControllerKey) as!
            ZoomViewController
            let toVC = transitionContext.viewControllerForKey(
                UITransitionContextToViewControllerKey) as!
            FlickrPhotoCollectionViewController
            
            transitionContext.containerView()?.addSubview(toVC.view)
            
            let animation = CABasicAnimation(keyPath: "transform")
            animation.fromValue = NSValue(CATransform3D:
                CATransform3DIdentity)
            animation.toValue = NSValue(CATransform3D:
                CATransform3DConcat(
                    CATransform3DMakeTranslation(0.0, -10.0, 0.0),
                    CATransform3DMakeScale(150.0, 150.0, 1.0)
                )
            )
            
            animation.duration = animationDuration
            animation.delegate = self
            animation.fillMode = kCAFillModeForwards
            animation.removedOnCompletion = false
            animation.timingFunction = CAMediaTimingFunction(name:
                kCAMediaTimingFunctionEaseIn)
            
            toVC.maskLayer.addAnimation(animation, forKey: nil)
            fromVC.maskLayer.addAnimation(animation, forKey: nil)
        }
        
        
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if let context = storedContext {
            context.completeTransition(!context.transitionWasCancelled())
            
            if operation == .Push {
                let fromVC = context.viewControllerForKey(
                    UITransitionContextFromViewControllerKey) as!
                FlickrPhotoCollectionViewController
                fromVC.maskLayer.removeAllAnimations()
            }else{
                let fromVC = context.viewControllerForKey(
                    UITransitionContextFromViewControllerKey) as!
                ZoomViewController
                fromVC.maskLayer.removeAllAnimations()
            }
            
            
        }
        storedContext = nil
    }
}
