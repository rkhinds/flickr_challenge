//
//  AppConstants.swift
//  flickr_challenge
//
//  Created by Romaine Hinds on 9/8/16.
//  Copyright © 2016 Romaine Hinds. All rights reserved.
//

import UIKit


let kAppDelegate        = UIApplication.sharedApplication().delegate as! AppDelegate
let kAPP_NAME           = "flickr_challenge"
let kApiKeyHeaderValue  = "api_key"
let kFormatheaderValue  = "json"

let kDidStartUpdatingLocationNotification   = "DidStartLocationManagerNotification"
let kDidFailToUpdateLocationNotification    = "LocationManagerDidFailWithErrorNotification"

class FlickrAPI {
    
    static let API_KEY                          = "a15989c5dee00fe83e88854bd514a4a2"
    static let API_SECRET                       = "fcd57e063f3a30d3"
    static let BASE_URL                         = "https://api.flickr.com/services/rest/"
    
    static let PhotoSearchMethod                = "flickr.photos.search"
    static let PhotoGetGeoLocationMethod        = "flickr.photos.geo.getLocation"
    static let PhotoGetInfoMethod               = "flickr.photos.getInfo"
    
    //https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
    class func getLinkToStaticPhoto(farmId: String, serverId: String, id: String, secret: String, size: FlickrPhotoSize) -> String {
        return "https://farm\(farmId).staticflickr.com/\(serverId)/\(id)_\(secret)_\(size.rawValue).jpg"
    }
    
    
}

extension FlickrAPI {
    enum FlickrHeaderKey: String {
        case APIKEY = "api_key",
        Format = "format"
    }
    
    enum FlickrPhotoSize: String {
        case Thumbnail = "t",
        Small = "m",
        Medium = "z",
        MidLarge = "b",
        Large = "h"
    }
}

func += <K, V> (inout left: [K:V], right: [K:V]) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}

extension UIScrollView {
    /// Sets content offset to the top.
    func resetScrollPositionToTop() {
        self.contentOffset = CGPoint(x: -contentInset.left, y: -contentInset.top)
    }
}